-- Transaction

-- commit
set autocommit = 0;
start transaction;
insert into recharge_plan(price, data, validity, calls, sms, additional_benefits, recharge_type_id) 
values(4999, 'unlimited', '12 Months', 'Unlimited Local,STD & Roaming calls', '100/Day', 'netflix (1 Year), Disney Hotstar(1 Year),Wynk Music Free, Free Hellotunes', 2);

update recharge_order set plan_id = 22 where id=1;
commit;

-- rollback

start transaction;
delete from recharge_plan where id = 24;
update recharge_plan set price = 1999 where id=24;
rollback;

-- delete from recharge_plan where id = 25;

start transaction;
insert into recharge_plan(price, data, validity, calls, sms, additional_benefits, recharge_type_id) 
values(4999, 'unlimited', '12 Months', 'Unlimited Local,STD & Roaming calls', '100/Day', 'netflix (1 Year), Disney Hotstar(1 Year),Wynk Music Free, Free Hellotunes', 2);


update recharge_order set plan_id = 23 where id=7;
rollback;
