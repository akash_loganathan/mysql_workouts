use airtel;
drop procedure usp_InsertShopDetailsWithArgs;

-- insert with input parameter
DELIMITER //
create procedure usp_InsertShopDetailsWithArgs(in shop_name varchar(20), in owner_name varchar(50), in city varchar(20))
begin
insert into franchise_shop(name, owner_name, city) values(shop_name, owner_name, city);
end //
DELIMITER ;

call usp_InsertShopDetailsWithArgs('venkat mobiles','venkat ramanan','Chennai');
select * from franchise_shop;


-- select with output parameter
drop procedure if exists usp_GetCountOfShopBYName;
DELIMITER //
create procedure usp_GetCountOfShopBYName(out count_of_shop int)
begin
select count(name) into count_of_shop from franchise_shop;
end //
DELIMITER ;

call usp_GetCountOfShopBYName(@count_of_shop);
select @count_of_shop;

-- inout parameter
drop procedure if exists usp_GetPlanIdBYOrderId;
DELIMITER //
create procedure usp_GetPlanIdBYOrderId(inout order_id int)
begin
select plan_id into order_id from broadband_order where id = order_id;
end //
DELIMITER ;
set @order_id = 4;
call usp_GetPlanIdBYOrderId(@order_id);
select @order_id as plan_id;
