create database employee;
use employee;
drop table employee_details;

-- primary, foreign, unique keys and not null

create table employee_details(id int primary key not null auto_increment, name varchar(100) not null, phone_number bigint, email varchar(100), unique(email), account_number varchar(100), unique(account_number));
create table employee_salary (id int primary key not null auto_increment, account_number varchar(100) not null, foreign key(account_number) references employee_details(account_number), salary float(8));
desc employee_details;
desc employee_salary;

insert into employee_details(name, phone_number, email, account_number) values('Alex', 9966778855, 'alex@email.com', '00101'),
('Bob', 9966778866, 'bob@yahoo.com', '00202'),
('Charles', 9966777855, 'charles@qedtek.com', '00303'),
('Dora', 9966778655, 'dora@microsoft.com', '00404');
select * from employee_details;

insert into employee_details(name, phone_number, email, account_number) values('Michael', 9566778855, 'michael@email.com', '00505');
insert into employee_details(name, phone_number, email, account_number) values('Mani', 9566778845, 'mani@email.com', '00606');
insert into employee_details(name, phone_number, email, account_number) values('john', 9766777845, 'john@email.com', '00707');

insert into employee_salary(account_number, salary) values('00101', 10000.00),
('00202', 10000.00),
('00303', 10000.00),
('00404', 10000.00);
select * from employee_salary;


-- check
alter table employee_salary
modify salary float(8) check (salary >= 10000.00);
insert into employee_salary(account_number, salary) values('00606', 10000.00);
insert into employee_salary(account_number, salary) values('00707', 10000.00);

-- default
alter table employee_details
add gender varchar(100) default 'male';
desc employee_details;
select * from employee_details;

use airtel;
show tables;



