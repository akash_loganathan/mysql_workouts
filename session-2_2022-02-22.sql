create database airtel;
use airtel;
create table recharge_plan (id int primary key auto_increment not null,
price float(8),
data varchar(100), 
validity varchar(100), 
calls varchar(200), 
sms varchar(100), 
subscription varchar(200), 
plan varchar(100));

desc recharge_plan;
insert into recharge_plan (price, data, validity, calls, sms, subscription, plan) values(719, '1.5GB/Day', '84 Days', 'Unlimited Local,STD & Roaming calls', '100/Day', 'amazon prime(Mobile Edition),Wynk Music Free, Free Hellotunes', 'Prepaid');
select * from recharge_plan;
insert into recharge_plan (price, data, validity, calls, sms, subscription, plan) values(479, '1.5GB/Day', '56 Days', 'Unlimited Local,STD & Roaming calls', '100/Day', 'Rs.100 cashback on FASTag, Wynk Music Free, Free Hellotunes', 'Prepaid'),
 (299, '1.5GB/Day', '28 Days', 'Unlimited Local,STD & Roaming calls', '100/Day', 'Xstream Mobile Pack, Wynk Music Free, Free Hellotunes', 'Prepaid'),
 (265, '1GB/Day', '28 Days', 'Unlimited Local,STD & Roaming calls', '100/Day', 'amazon prime(Mobile Edition), Wynk Music Free, Free Hellotunes', 'Prepaid'),
 (839, '2GB/Day', '84 Days', 'Unlimited Local,STD & Roaming calls', '100/Day', 'Xstream Mobile Pack, amazon prime(Mobile Edition), Wynk Music Free, Free Hellotunes', 'Prepaid'),
 (399, '40GB', '1 Month', 'Unlimited Local,STD & Roaming calls', '100/Day', 'AirtelTv & amazon prime(1 Year), Wynk Music Free, Free Hellotunes', 'Postpaid'),
 (499, '75GB', '1 Month', 'Unlimited Local,STD & Roaming calls', '100/Day', 'AirtelTv, Netflix(3 Month), Wynk Music Free, Free Hellotunes', 'Postpaid'),
 (649, '90GB', '1 Month', 'Unlimited Local,STD & Roaming calls', '100/Day', 'AirtelTv & amazon prime(1 Year), Netflix(3 Month), Wynk Music Free, Free Hellotunes', 'Postpaid'),
 (799, '100GB', '1 Month', 'Unlimited Local,STD & Roaming calls', '100/Day', 'AirtelTv & amazon prime(1 Year), Netflix(3 Month),Zee5 Free, Wynk Music Free, Free Hellotunes', 'Postpaid'),
 (649, '90GB', '1 Month', 'Unlimited Local,STD & Roaming calls', '100/Day', 'AirtelTv & amazon prime(1 Year), Netflix(3 Month), Wynk Music Free, Free Hellotunes', 'Postpaid'),
 (179, '2GB', '28 Days', 'Unlimited Local,STD & Roaming calls', '300', 'Wynk Music Free, Free Hellotunes', 'Prepaid'),
 (549, '2GB/Day', '56 Days', 'Unlimited Local,STD & Roaming calls', '100/Day', 'amazon prime(Mobile Edition), Apollo 24|7 Circle(3 Months), Wynk Music Free, Free Hellotunes', 'Prepaid');
 select * from recharge_plan;
 insert into recharge_plan (price, data, validity, calls, sms, subscription, plan) values(155, '1GB', '24 Days', 'Unlimited Local,STD & Roaming calls', '300', 'amazon prime(Mobile Edition),Wynk Music Free, Free Hellotunes', 'Prepaid'),
 (209, '1GB/Day', '21 Days', 'Unlimited Local,STD & Roaming calls', '100/Day', 'amazon prime(Mobile Edition),Wynk Music Free, Free Hellotunes', 'Prepaid'),
 (449, '2.5GB/Day', '28 Days', 'Unlimited Local,STD & Roaming calls', '100/Day', 'Xstream(28 Days),amazon prime(Mobile Edition),Wynk Music Free, Free Hellotunes', 'Prepaid'),
 (2394, 'unlimited', '6 Months', 'Unlimited Local,STD & Roaming calls', '100/Day', 'amazon prime(6 Months), Disney Hotstar(6 Months),Wynk Music Free, Free Hellotunes', 'Postpaid'),
 (4194, 'unlimited', '6 Months', 'Unlimited Local,STD & Roaming calls', '100/Day', 'amazon prime(6 Months), Netflix(6 Months),Wynk Music Free, Free Hellotunes', 'Postpaid'),
 (4788, 'unlimited', '12 Months', 'Unlimited Local,STD & Roaming calls', '100/Day', 'amazon prime(1 Year), Disney Hotstar(1 Year),Wynk Music Free, Free Hellotunes', 'Postpaid'),
 (8388, 'unlimited', '12 Months', 'Unlimited Local,STD & Roaming calls', '100/Day', 'amazon prime(1 Year), Netflix(1 Year),Wynk Music Free, Free Hellotunes', 'Postpaid');
  select * from recharge_plan;
 