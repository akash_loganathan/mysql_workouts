-- session 2.2

-- Alter
 -- add
use airtel;
alter table recharge_plan
add column speed varchar(100) not null;
desc recharge_plan;

-- change
alter table recharge_plan
change subscription additional_benifits varchar(200) not null;
desc recharge_plan;

-- modify
alter table recharge_plan
modify price float(8) not null, 
modify data varchar(100) not null,
modify validity varchar(100) not null,
modify calls varchar(100) not null,
modify sms varchar(100) not null,
modify additional_benifits varchar(200),
modify plan varchar(100) not null;
desc recharge_plan;

alter table recharge_plan
change additional_benifits additional_benefits varchar(200);
desc recharge_plan;

-- drop
alter table recharge_plan
drop speed;
desc recharge_plan;

-- Drop Database
create database grocery;
use grocery;
create table product(id int primary key not null auto_increment, name varchar(100) not null);
create table customer (id int primary key not null auto_increment, name varchar(100) not null);
desc customer;

-- DML
-- insert
insert into product(name) values('santoor'),('medimix'),('sunflower oil');
select * from product;
insert into customer(name) values('alex'),('adam'),('michael');
select * from customer;

-- trucate
truncate product;
select * from product;

-- DML
-- delete
delete from customer where id between 5 and 7;
select * from customer;

-- DML 
-- update
update product set name = 'ruchi gold' where id = 3;
select * from product;

-- drop database
drop database grocery;



 
  
 