use airtel;
 drop table data_types_reference;
create table data_types_reference(id int primary key not null auto_increment,
data char(4) not null,
additional_benefits varchar(100) not null, 
description text(200) not null,
membership tinyint not null,
phone_number bigint not null,
price float8 not null,
cost decimal(9,3) not null,
connection_date_time datetime not null);

desc data_types_reference;

insert into data_types_reference(data,
additional_benefits,
description,
membership,
phone_number,
price,
cost,
connection_date_time)

values ('1gb',
'Rs.100 cashback on FASTag, Wynk Music Free, Free Hellotunes',
'Welcome to Airtel postpaid plans, your one-stop for all network solutions. 
Offering Airtel 4G postpaid plans, with tons of perks. 
We’ll give your family a single bill and single plan that’ll help save up to 25% monthly.
Unlimited Local/STD & National Roaming calls. 
All you have to do is – select the plan that best suits you and we will deliver the sim at your doorstep.',
1, 
9988776655, 
1000.00, 
399.99, 
'2021-12-06 01:30'),

('10gb',
'AirtelTv, Netflix(3 Month), Wynk Music Free, Free Hellotunes',
'Step into the future of unlimited broadband plans with Airtel fibernet technology. 
Experience the high-speed internet on our existing Fiber broadband connection in Hyderabad across multiple devices.',
0, 
9988779955, 
500.12, 
799.92, 
'2021-01-06 05:30'),

('5gb',
'amazon prime(Mobile Edition),Wynk Music Free, Free Hellotunes',
'Airtel Xstream Fiber offers you with Fiber optic internet connection transforming your daily broadband connection experience with high-speed internet. 
Airtel allows you to enjoy the fastest broadband connection in Hyderabad with the speed of up to 1 Gbps, which means once you connect broadband, you will get faster downloads and less buffering.',
1, 
9358779955, 
440.12, 
899.921, 
'2021-01-08 02:30');

select * from data_types_reference;