-- Join
use airtel;
create table recharge_order(id int not null primary key auto_increment, customer_id int not null, foreign key(customer_id) references recharge_customer(id), plan_id int not null, foreign key(plan_id) references recharge_plan(id));
desc recharge_order;

-- alter table recharge_customer
-- drop column recharge_type_id;
desc recharge_customer;
select * from recharge_customer;

-- alter table recharge_plan
-- drop column plan;
select * from recharge_plan;

-- alter table recharge_plan 
-- add recharge_type_id int, add foreign key fk_recharge_plan_type(recharge_type_id) references recharge_type(id);
desc recharge_plan;

update recharge_plan
set recharge_type_id = (select id from recharge_type where code = 'Prepaid') where id between 2 and 15;
update recharge_plan
set recharge_type_id = (select id from recharge_type where code = 'Postpaid') where id between 16 and 19;
select id from recharge_type where code = 'Prepaid';

insert into recharge_order(customer_id, plan_id) values
(4,15),
(1,7),
(3,18),
(2,19);

-- Inner Join

select recharge_order.id, 
recharge_customer.name as customer, 
recharge_type.code as plan_name,
recharge_plan.price

from recharge_order
inner join recharge_customer
on recharge_order.customer_id = recharge_customer.id
inner join recharge_plan
on recharge_order.plan_id = recharge_plan.id
inner join recharge_type
on recharge_plan.recharge_type_id = recharge_type.id
order by recharge_order.id;