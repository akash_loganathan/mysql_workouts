use airtel;
 -- left outer join
 
select broadband_customer.name as customer_name, 
broadband_plan.name as plan_name, 
broadband_plan.price,
broadband_order.connection_date

from broadband_customer
left outer join broadband_order
on broadband_order.customer_id = broadband_customer.id
left outer join broadband_plan
on broadband_order.plan_id = broadband_plan.id;

-- right outer join

select recharge_order.id as order_id, 
recharge_customer.name as customer_name,
recharge_type.code as plan_name, 
recharge_plan.price

from recharge_order
right join recharge_customer
on recharge_order.customer_id = recharge_customer.id
right join recharge_plan
on recharge_order.plan_id = recharge_plan.id
right join recharge_type
on recharge_plan.recharge_type_id = recharge_type.id order by recharge_order.id desc;



 
 