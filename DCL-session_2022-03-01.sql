-- session-4
-- DCL- Data Control Language

create database franchise;
use franchise;
create table shop(id int primary key auto_increment not null, name varchar(100) not null,owner_name varchar(100) not null, city varchar(100) not null);
desc shop;
insert into shop(name, owner_name, city) values('A.K Mobiles', 'Anil Kumar', 'Bangalore'),
('M.S Mobiles', 'Muthu Samy', 'Chennai'),
('Harish Mobiles', 'Harish', 'Delhi');
select * from shop;

create table broadband_plan(id int not null primary key auto_increment, 
name varchar(100) not null unique, 
internet varchar(100) not null, 
speed varchar(100) not null, 
calls varchar(100) not null, 
additional_benefits varchar(200), 
price float(8) not null, validity varchar(100) not null);
desc broadband_plan;

insert into broadband_plan(name, internet, speed, calls, additional_benefits, price, validity) 
			values('Entertainment','Unlimited','Up to 200Mbps', 'Unlimited Local/STD Calls', 'Amazon Prime, Wynk Music, Shaw Academy',999, '1 Month'),
				  ('Basic','Unlimited','Up to 40Mbps', 'Unlimited Local/STD Calls', 'Wynk Music, Shaw Academy',499, '1 Month'),
                  ('Standard','Unlimited','Up to 100Mbps', 'Unlimited Local/STD Calls', 'Wynk Music, Shaw Academy',799, '1 Month'),
                  ('Professional','Unlimited','Up to 300Mbps', 'Unlimited Local/STD Calls', 'Amazon Prime, Wynk Music, Shaw Academy',1499, '1 Month'),
                  ('Infinity','Unlimited','Up to 1Gbps', 'Unlimited Local/STD Calls', 'Amazon Prime, Wynk Music, Shaw Academy',3999, '1 Month');
select * from broadband_plan;

-- DCL
-- DCL commands are used to grant and take back authority from any database user.

-- Users

create user akash_01@ 'localhost' identified by '';
create user akash_02@ 'localhost' identified by '';
create user akash_03@ 'localhost' identified by '';
create user akash_04@ 'localhost' identified by '';
create user akash_05@ 'localhost' identified by '';
create user akash_06@ 'localhost' identified by '';

-- access another database

-- Grant: It is used to give user access privileges to a database.

-- global all access
grant all on *.* to akash_01@'localhost';
show grants for 'akash_01'@'localhost';

-- database access
grant all on airtel .* to akash_02@'localhost';
show grants for 'akash_02'@'localhost';

-- table access
grant all on airtel.broadband_plan to akash_03@'localhost';
show grants for 'akash_03'@'localhost';

-- column access
grant select,update,delete,insert(name, additional_benefits, price) on airtel.broadband_plan to akash_04@'localhost';
show grants for 'akash_04'@'localhost';

-- proxy access
grant proxy on  'akash_04'@'localhost' to  'akash_05'@'localhost';
show grants for 'akash_05'@'localhost';

-- revoke: It is used to take back permissions from the user.

grant proxy on  'akash_05'@'localhost' to  'akash_06'@'localhost';
show grants for 'akash_06'@'localhost';

revoke proxy on 'akash_05'@'localhost' from 'akash_06'@'localhost';
show grants for 'akash_06'@'localhost';




-- access same database

-- Grant: It is used to give user access privileges to a database.

-- global all access
grant all on *.* to akash_01@'localhost';
show grants for 'akash_01'@'localhost';

-- database access
grant all on franchise .* to akash_02@'localhost';
show grants for 'akash_02'@'localhost';

-- table access
grant all on franchise.broadband_plan to akash_03@'localhost';
show grants for 'akash_03'@'localhost';

-- column access
grant select,update,delete,insert(name, additional_benefits, price) on franchise.broadband_plan to akash_04@'localhost';
show grants for 'akash_04'@'localhost';

-- proxy access
grant proxy on  'akash_04'@'localhost' to  'akash_05'@'localhost';
show grants for 'akash_05'@'localhost';

-- revoke: It is used to take back permissions from the user.

grant proxy on  'akash_05'@'localhost' to  'akash_06'@'localhost';
show grants for 'akash_06'@'localhost';

revoke proxy on 'akash_05'@'localhost' from 'akash_06'@'localhost';
show grants for 'akash_06'@'localhost';


