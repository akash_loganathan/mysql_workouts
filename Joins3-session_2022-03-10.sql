use airtel;
 -- full outer join
 -- union
select * from broadband_customer a left join broadband_order b
on a.id = b.customer_id
union 
select * from broadband_customer a right join broadband_order b
on a.id = b.customer_id;
-- ---

 -- full outer join
 -- union all
select * from broadband_customer a left join broadband_order b
on a.id = b.customer_id
union all
select * from broadband_customer a right join broadband_order b
on a.id = b.customer_id;


-- cross join
-- cartesian:
 		-- two sets a={x,y,z}, b={1,2,3}
		-- a x b = {(x,1),(x,2),(x,3),(y,1),(y,2),(y,3),(z,1),(z,2),(z,3)}

create table franchise_shop(id int primary key auto_increment not null, 
name varchar(100) not null,
owner_name varchar(100) not null, 
city varchar(100) not null);
desc franchise_shop;

insert into franchise_shop(name, owner_name, city) values('A.K Mobiles', 'Anil Kumar', 'Bangalore'),
('M.S Mobiles', 'Muthu Samy', 'Chennai'),
('Harish Mobiles', 'Harish', 'Delhi'),
('Thai Mobiles', 'Mugesh', 'Hyderabad');
select * from franchise_shop;

select franchise_shop.name as shop_name, broadband_plan.name as plan_name, broadband_plan.price as plan_price
from franchise_shop
cross join broadband_plan;






 
 