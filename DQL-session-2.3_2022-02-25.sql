-- session 2.3

use airtel;
create table broadband_plan(id int not null primary key auto_increment, 
name varchar(100) not null unique, 
internet varchar(100) not null, 
speed varchar(100) not null, 
calls varchar(100) not null, 
additional_benefits varchar(200), 
price float(8) not null, validity varchar(100) not null);
desc broadband_plan;

insert into broadband_plan(name, internet, speed, calls, additional_benefits, price, validity) 
			values('Entertainment','Unlimited','Up to 200Mbps', 'Unlimited Local/STD Calls', 'Amazon Prime, Wynk Music, Shaw Academy',999, '1 Month'),
				  ('Basic','Unlimited','Up to 40Mbps', 'Unlimited Local/STD Calls', 'Wynk Music, Shaw Academy',499, '1 Month'),
                  ('Standard','Unlimited','Up to 100Mbps', 'Unlimited Local/STD Calls', 'Wynk Music, Shaw Academy',799, '1 Month'),
                  ('Professional','Unlimited','Up to 300Mbps', 'Unlimited Local/STD Calls', 'Amazon Prime, Wynk Music, Shaw Academy',1499, '1 Month'),
                  ('Infinity','Unlimited','Up to 1Gbps', 'Unlimited Local/STD Calls', 'Amazon Prime, Wynk Music, Shaw Academy',3999, '1 Month');
select * from broadband_plan;

create table broadband_customer(id int not null primary key auto_increment,
name varchar(100) not null,
plan_name varchar(100) not null, foreign key(plan_name) references broadband_plan(name),
price float(8) not null,
phone_number bigint not null,
city varchar(100) not null,
connection_date date not null);

insert into broadband_customer(name, plan_name,price, phone_number, city, connection_date) 
			values('Vijay Mallaya','Basic',499,'9911223344', 'Bangalore', '2021-12-05');
select * from broadband_customer;

insert into broadband_customer(name, plan_name,price, phone_number, city, connection_date) 
			values('Nirav Modi','Standard',799, '9912223344', 'Delhi', '2021-12-06'),
				  ('Nishan','Basic',499,'9913223344', 'Chennai', '2021-12-10'),
                  ('Akash','Entertainment',999,'9911213344', 'Delhi', '2021-12-15'),
                  ('Rajiv','Standard',799, '9911223344', 'Delhi', '2021-12-17'),
                  ('Rudhra','Basic',499,'9911221344', 'Hyderabad', '2021-12-20'),
                  ('Gaurav','Entertainment',999,'9911222344', 'Chennai', '2021-12-28'),
                  ('Ramesh','Infinity',3999,'9911223144', 'Bangalore', '2022-01-14'),
                  ('Suresh','Basic',499,'9911223544', 'Delhi', '2022-01-08'),
                  ('Sarath','Infinity',3999,'9911223314', 'Delhi', '2022-01-11'),
                  ('Karan','Professional',1499,'9911223342', 'Chennai', '2022-01-16'),
                  ('Lakshmi','Basic',499,'9211223344', 'Delhi', '2022-01-22'),
                  ('Lalitha','Standard',799, '9311223344', 'Bangalore', '2022-01-25'),
                  ('Raja','Professional',1499,'9911523344', 'Delhi', '2022-01-28'),
				  ('Manikandan','Infinity',3999,'9711523344', 'Hyderabad', '2022-02-03'),
                  ('Vinay','Standard',799, '9811227344', 'Chennai', '2022-02-05'),
                  ('Asiba','Entertainment',999,'9988223344', 'Delhi', '2022-02-07'),
                  ('Elakiya','Standard',799, '9976223344', 'Chennai', '2022-02-12'),
                  ('Savitha','Basic',499,'9938223344', 'Delhi', '2022-02-14'),
                  ('Ritesh','Entertainment',999,'9911993344', 'Bangalore', '2022-02-16'),
                  ('Natasha','Infinity',3999,'9910023344', 'Hyderabad', '2022-02-18'),
                  ('Jhon','Standard',799, '9911221044', 'Chennai', '2022-02-21'),
                  ('Scarlett','Basic',499,'9900123344', 'Delhi', '2022-02-24');

select * from broadband_customer;

-- Last Month records

select * from broadband_customer where month(connection_date) = '02';
select * from broadband_customer where month(connection_date) = month(current_date());

-- before month
select * from broadband_customer where month(connection_date) = month(now() - interval 1 month);

-- Last 10 - 15 records

select * from broadband_customer order by id desc limit 10, 5; 

-- select using column & where

select name from broadband_customer where plan_name = 'Basic';

-- Group by & Having 
-- Aggregate function

-- count() using group by

select plan_name,count(plan_name) as count from broadband_customer group by plan_name;
select city,count(city) as count from broadband_customer group by city;

-- sum() using group by
select plan_name, sum(price) as total_price from broadband_customer group by plan_name;
select city, sum(price) as total_price from broadband_customer group by city;

-- Using Having

-- city based
select plan_name,city,count(plan_name) as total_plan from broadband_customer group by plan_name, city having city = 'Chennai';
select plan_name,city,count(plan_name) as total_plan from broadband_customer group by plan_name, city having city = 'Hyderabad';

-- plan based
select plan_name,city,count(plan_name) as total_plan from broadband_customer group by plan_name, city having plan_name = 'Basic';
select plan_name,city,count(plan_name) as total_plan from broadband_customer group by plan_name, city having plan_name = 'Entertainment';

-- having with sum()
select city, sum(price) as total_price from broadband_customer group by city having city = 'Chennai';


-- max()
select plan_name,city,max(plan_name) as max_plan from broadband_customer group by plan_name,city having city = 'Chennai';

-- min()
select min(plan_name) as min_plan from broadband_customer group by plan_name;

-- avg()
select avg(price) as avg_sale from broadband_customer;
