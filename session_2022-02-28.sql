use airtel;
create table recharge_type (id int not null primary key auto_increment, code varchar(50) not null, description varchar(100) not null);
desc recharge_type;
insert into recharge_type(code, description) values('Prepaid','prepayment recharge'),
('Postpaid','Postpayment recharge'),
('Broadband','Airtel fiber recharge'),
('DTH','Airtel DTH connection recharge');
select * from recharge_type;

create table recharge_customer(id int not null primary key auto_increment, name varchar(100), recharge_type_id int not null, recharge_date_time datetime);

insert into recharge_customer(name,recharge_type_id, recharge_date_time) values('Rahul', (select id from
recharge_type where code ='Prepaid') ,'2022-01-05 12:35:29');

select * from recharge_customer;

insert into recharge_customer(name,recharge_type_id, recharge_date_time) values('Sarath', (select id from
recharge_type where code ='Broadband') ,'2022-01-07 13:35:29'),
('Karan', (select id from
recharge_type where code ='DTH') ,'2022-01-12 01:35:29'),
('Mugesh', (select id from
recharge_type where code ='Postpaid') ,'2022-01-20 02:35:29');

Select recharge_customer.name,recharge_type.code from recharge_customer, recharge_type
Where recharge_customer.recharge_type_id = recharge_type.id;