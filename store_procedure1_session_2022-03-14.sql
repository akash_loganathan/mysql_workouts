use airtel;

-- stored procedure

-- select
DELIMITER //
create procedure usp_GetOrderDetails() 
begin
select recharge_order.id, 
recharge_customer.name as customer, 
recharge_type.code as plan_name,
recharge_plan.price

from recharge_order
inner join recharge_customer
on recharge_order.customer_id = recharge_customer.id
inner join recharge_plan
on recharge_order.plan_id = recharge_plan.id
inner join recharge_type
on recharge_plan.recharge_type_id = recharge_type.id
order by recharge_order.id;
end//
DELIMITER ;

call usp_GetOrderDetails;

-- insert
DELIMITER //
create procedure usp_InsertShopDetails()
begin
insert into franchise_shop(name, owner_name, city) values('ABC Mobiles', 'Jeya Pradeep', 'chennai');
end //
DELIMITER ;

call usp_InsertShopDetails;
select * from franchise_shop;

-- update
DELIMITER //
create procedure usp_UpdateShopDetailsById()
begin
update franchise_shop set owner_name = 'Murugan' where id= 4;
end //
DELIMITER ;

call usp_UpdateShopDetailsById;
select * from franchise_shop;

-- delete
DELIMITER //
create procedure usp_DeleteShopDetailsById()
begin
delete from franchise_shop where id= 5;
end //
DELIMITER ;

call usp_DeleteShopDetailsById;
select * from franchise_shop;
