-- Subquery
use airtel;
show tables;

-- scalar
select name from broadband_customer where id = (select id from broadband_customer where phone_number = '9911223342'); 

-- Row
select * from broadband_customer where id = (select id from broadband_customer where name = 'Karan'); 
select id from broadband_customer where name = 'Karan';

-- All
select * from broadband_customer where plan_name = all (select name from broadband_plan where name = 'Basic'); 
select name from broadband_plan where name = 'Basic';

-- Any
select * from broadband_customer where price = any (select price from broadband_plan where price = 999); 

-- Exists
select * from broadband_customer where exists (select price from broadband_plan where price = 999);

-- Not Exists
select * from broadband_customer where !exists (select price from broadband_plan where price = 99);






